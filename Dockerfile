FROM java:8
EXPOSE 8090
ADD /service/target/user-microservice-0.0.1-SNAPSHOT.jar user-microservice-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","user-microservice-0.0.1-SNAPSHOT.jar"]